﻿namespace EmployeeManagement.Dto;

public class AddEmployeeToProject
{
    /// <summary>
    /// Guid of employee that is going to be added or removed from a project
    /// </summary>
    /// <example>5407627b-1402-4e4b-b913-3994f825ddde</example>
    public Guid EmployeeId { get; set; }
}