﻿using EmployeeManagement.Data;
using EmployeeManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Controllers
{
    [Route("api/technologies")]
    [ApiController]
    public class TechnologiesController : Controller
    {
        private readonly AppDbContext _dbContext;
    
        public TechnologiesController(AppDbContext _dbContext)
        {
            this._dbContext = _dbContext;
        }
    
        [HttpGet]
        public async Task<IActionResult> GetAllTechnologies()
        {
            var technologies = await _dbContext.Technologies.ToListAsync();
    
            return Ok(technologies);
        }
    
        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetSingleTechnology([FromRoute] Guid id)
        {
            var technology = await _dbContext.Technologies.FirstOrDefaultAsync(x => x.Id == id);
    
            if (technology is null)
            {
                return NotFound();
            }
    
            return Ok(technology);
        }
    
        [HttpPost]
        public async Task<IActionResult> AddTechnology([FromBody] Technology technology)
        {
            technology.Id = Guid.NewGuid();
    
            await _dbContext.Technologies.AddAsync(technology);
            await _dbContext.SaveChangesAsync();
    
            return Ok(technology);
        }
    
        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> UpdateTechnology([FromBody] Technology technologyRequest, [FromRoute] Guid id)
        {
            var technology = await _dbContext.Technologies.FindAsync(id);
    
            if (technology is null)
            {
                return NotFound();
            }
    
            technology.Name = technologyRequest.Name;
    
            await _dbContext.SaveChangesAsync();
    
            return Ok(technology);
        }
    
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> DeleteTechnology([FromRoute] Guid id)
        {
            var technology = await _dbContext.Technologies.FindAsync(id);
    
            if (technology is null)
            {
                return NotFound();
            }
    
            _dbContext.Technologies.Remove(technology);
    
            await _dbContext.SaveChangesAsync();
    
            return Ok();
        }
    }
}

