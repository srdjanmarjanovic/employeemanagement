using System.Text.Json;
using System.Text.Json.Serialization;
using EmployeeManagement.Data;
using EmployeeManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EmployeeManagement.Controllers
{
    [Route("api/projects")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly AppDbContext _dbContext;

        public ProjectsController(AppDbContext _dbContext)
        {
            this._dbContext = _dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProjects()
        {
            var projects = await _dbContext.Projects.ToListAsync();
            
            return Ok(projects);
        }

        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetSingleProject([FromRoute] Guid id)
        {
            var project = await _dbContext.Projects.FirstOrDefaultAsync(x => x.Id == id);

            if (project is null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        [HttpPost]
        public async Task<IActionResult> AddProject([FromBody] Project project)
        {
            project.Id = Guid.NewGuid();

            await _dbContext.Projects.AddAsync(project);
            await _dbContext.SaveChangesAsync();
            
            return Ok(project);
        }

        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> UpdateProject([FromBody] Project projectRequest, [FromRoute] Guid id)
        {
            var project = await _dbContext.Projects.FindAsync(id);

            if (project is null)
            {
                return NotFound();
            }

            project.Name = projectRequest.Name;
            project.Budget = projectRequest.Budget;
            project.Description = projectRequest.Description;

            await _dbContext.SaveChangesAsync();

            return Ok(project);
        }
        
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> DeleteProject([FromRoute] Guid id)
        {
            var project = await _dbContext.Projects.FindAsync(id);

            if (project is null)
            {
                return NotFound();
            }

            _dbContext.Projects.Remove(project);

            await _dbContext.SaveChangesAsync();
            
            return Ok();
        }
    }
}
