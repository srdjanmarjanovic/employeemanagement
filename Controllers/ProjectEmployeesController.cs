using System.Text.Json.Nodes;
using EmployeeManagement.Data;
using EmployeeManagement.Dto;
using EmployeeManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Controllers
{
    [Route("api/projects/{projectId:Guid}/employees")]
    [ApiController]
    public class ProjectEmployeesController : ControllerBase
    {
        private readonly AppDbContext _dbContext;

        public ProjectEmployeesController(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetProjectEmployees([FromRoute] Guid projectId)
        {
            var employees = await _dbContext.Projects.Where(p => p.Id == projectId).SelectMany(c => c.Employees).ToListAsync();

            return Ok(employees);
        }
        
        [HttpPost]
        public async Task<IActionResult> AddProjectEmployee([FromRoute] Guid projectId, AddEmployeeToProject addEmployeeToProjectRequest)
        {
            var project = await _dbContext.Projects.FirstOrDefaultAsync(p => p.Id == projectId);
            var employee = await _dbContext.Employees.FirstOrDefaultAsync(e => e.Id == addEmployeeToProjectRequest.EmployeeId);

            if (employee is null || project is null)
            {
                return NotFound();   
            }

            if (project.Employees.Contains(employee))
            {
                return BadRequest("Employee is already on this project");
            }
            
            project.Employees.Add(employee);
                
            await _dbContext.SaveChangesAsync();
            
            return Ok();
        }
        
        [HttpDelete]
        [Route("{employeeId:Guid}")]
        public async Task<IActionResult> RemoveProjectEmployee([FromRoute] Guid projectId, [FromRoute] Guid employeeId)
        {
            var employee = await _dbContext.Employees.FirstOrDefaultAsync(e => e.Id == employeeId);
            var project = await _dbContext.Projects.Include(e => e.Employees).FirstOrDefaultAsync(p => p.Id == projectId);

            if (employee is null || project is null)
            {
                return NotFound();   
            }
            
            project.Employees.Remove(employee);
            _dbContext.SaveChanges();
            
            return Ok();
        }
    }
}
