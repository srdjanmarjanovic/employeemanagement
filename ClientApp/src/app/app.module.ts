import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeesListComponent } from './components/employees/employees-list/employees-list.component';
import {HttpClientModule} from "@angular/common/http";
import { AddEmployeeComponent } from './components/employees/add-employee/add-employee.component';
import {FormsModule} from "@angular/forms";
import { EditEmployeeComponent } from './components/employees/edit-employee/edit-employee.component';
import { ProjectsListComponent } from './components/projects/projects-list/projects-list.component';
import { TechnologiesListComponent } from './components/technologies/technologies-list/technologies-list.component';
import { TechnologyAddComponent } from './components/technologies/technology-add/technology-add.component';
import { TechnologyEditComponent } from './components/technologies/technology-edit/technology-edit.component';
import { ProjectAddComponent } from './components/projects/project-add/project-add.component';
import { ProjectViewComponent } from './components/projects/project-view/project-view.component';
import { ProjectEditComponent } from './components/projects/project-edit/project-edit.component';
import { ViewEmployeeComponent } from './components/employees/view-employee/view-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeesListComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    ProjectsListComponent,
    TechnologiesListComponent,
    TechnologyAddComponent,
    TechnologyEditComponent,
    ProjectAddComponent,
    ProjectViewComponent,
    ProjectEditComponent,
    ViewEmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
