﻿import {Employee} from "./emplyee.model";

export interface Project {
  id: string;
  name: string;
  description: string;
  budget: number;
}
