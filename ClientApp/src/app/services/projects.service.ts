import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Project} from "../models/project.model";
import {Employee} from "../models/emplyee.model";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  apiUrl: string = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) {}

  getAllProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.apiUrl + '/api/projects');
  }

  addProject(addProjectRequest: Project): Observable<Project> {
    addProjectRequest.id = '00000000-0000-0000-0000-000000000000';
    return this.http.post<Project>(this.apiUrl + '/api/projects', addProjectRequest)
  }

  getProject(projectId: string): Observable<Project> {
    return this.http.get<Project>(this.apiUrl + '/api/projects/' + projectId);
  }

  updateProject(editProjectRequest: Project): Observable<Project> {
    return this.http.put<Project>(this.apiUrl + '/api/projects/' + editProjectRequest.id, editProjectRequest)
  }

  deleteProject(projectId: string): Observable<Project> {
    return this.http.delete<Project>(this.apiUrl + '/api/projects/' + projectId)
  }

  getProjectEmployees(projectId: string): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.apiUrl + '/api/projects/' + projectId + '/employees');
  }

  removeEmployeeFromProject(projectId: string, employeeId: string): Observable<boolean> {
    return this.http.delete<boolean>(this.apiUrl + '/api/projects/' + projectId + '/employees/' + employeeId, );
  }
  addEmployeeToProject(projectId: string, employeeId: string): Observable<boolean> {
    return this.http.post<boolean>(this.apiUrl + '/api/projects/' + projectId + '/employees', {
      employeeId
    });
  }
}
