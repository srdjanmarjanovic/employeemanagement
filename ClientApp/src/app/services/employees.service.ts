import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Employee} from "../models/emplyee.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  apiUrl: string = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) {

  }

  getAllEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.apiUrl + '/api/employees');
  }

  addEmployee(addEmployeeRequest: Employee): Observable<Employee> {
    addEmployeeRequest.id = '00000000-0000-0000-0000-000000000000';
    return this.http.post<Employee>(this.apiUrl + '/api/employees', addEmployeeRequest)
  }

  getEmployee(employeeId: string): Observable<Employee> {
    return this.http.get<Employee>(this.apiUrl + '/api/employees/' + employeeId);
  }

  updateEmployee(editEmployeeRequest: Employee): Observable<Employee> {
    return this.http.put<Employee>(this.apiUrl + '/api/employees/' + editEmployeeRequest.id, editEmployeeRequest)
  }

  deleteEmployee(employeeId: string): Observable<Employee> {
    return this.http.delete<Employee>(this.apiUrl + '/api/employees/' + employeeId)
  }
}
