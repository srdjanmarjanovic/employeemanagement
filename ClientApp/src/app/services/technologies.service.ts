import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Technology} from "../models/technology.model";

@Injectable({
  providedIn: 'root'
})
export class TechnologiesService {
  apiUrl: string = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) {}

  getAllTechnologies(): Observable<Technology[]> {
    return this.http.get<Technology[]>(this.apiUrl + '/api/technologies');
  }

  addTechnology(addTechnologyRequest: Technology): Observable<Technology> {
    addTechnologyRequest.id = '00000000-0000-0000-0000-000000000000';
    return this.http.post<Technology>(this.apiUrl + '/api/technologies', addTechnologyRequest)
  }

  getTechnology(technologyId: string): Observable<Technology> {
    return this.http.get<Technology>(this.apiUrl + '/api/technologies/' + technologyId);
  }

  updateTechnology(editTechnologyRequest: Technology): Observable<Technology> {
    return this.http.put<Technology>(this.apiUrl + '/api/technologies/' + editTechnologyRequest.id, editTechnologyRequest)
  }

  deleteTechnology(technologyId: string): Observable<Technology> {
    return this.http.delete<Technology>(this.apiUrl + '/api/technologies/' + technologyId)
  }
}
