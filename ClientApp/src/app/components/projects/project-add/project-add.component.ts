import { Component } from '@angular/core';
import {ProjectsService} from "../../../services/projects.service";
import {Router} from "@angular/router";
import {Project} from "../../../models/project.model";

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent {
  addProjectRequest: Project = {
    id: '',
    name: '',
    description: '',
    budget: 0,
  }
  constructor(
    private projectService: ProjectsService,
    private router: Router
  ) {}

  addProject() {
    this.projectService.addProject(this.addProjectRequest)
      .subscribe({
        next: (project) => {
          this.router.navigate(['projects'])
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
