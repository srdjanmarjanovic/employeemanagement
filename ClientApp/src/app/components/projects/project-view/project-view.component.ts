import { Component } from '@angular/core';
import {Project} from "../../../models/project.model";
import {ProjectsService} from "../../../services/projects.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Employee} from "../../../models/emplyee.model";
import {EmployeesService} from "../../../services/employees.service";
import * as _ from 'lodash';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
export class ProjectViewComponent {
  project: Project = {
    id: '',
    name: '',
    description: '',
    budget: 0,
  };

  employeesOnProject: Employee[] = [];
  employeesNotOnProject: Employee[] = [];

  constructor(
    private projectsService: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private employeesService: EmployeesService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        const id = params.get('id');

        if (id) {
          this.loadProject(id);
          this.loadProjectEmployees(id);
          this.loadAllEmployees();
        }
      }
    });
  }

  loadProject(id: string) {
    this.projectsService.getProject(id).subscribe({
      next: (project) => {
        this.project = project;
      },
      error: (response) => {
        console.log(response);
      }
    });
  }

  loadProjectEmployees(id: string) {
    this.projectsService.getProjectEmployees(id).subscribe({
      next: (employees: Employee[]) => {
        this.employeesOnProject = employees;
      },
      error: (response) => {
        console.log(response);
      }
    });
  }

  loadAllEmployees() {
    this.employeesService.getAllEmployees().subscribe({
      next: (allEmployees: Employee[]) => {
        this.employeesNotOnProject = _.differenceBy(allEmployees, this.employeesOnProject, 'id');
      },
      error: (response) => {
        console.log(response);
      }
    });
  }

  removeEmployeeFromProject(employee: Employee) {
    this.projectsService.removeEmployeeFromProject(this.project.id, employee.id)
      .subscribe({
        next: () => {
          this.loadProjectEmployees(this.project.id);
          this.loadAllEmployees();
        },
        error: (response) => {
          console.log(response);
        }
      });
  }

  addEmployeeToProject(employee: Employee) {
    this.projectsService.addEmployeeToProject(this.project.id, employee.id)
      .subscribe({
        next: (response) => {
          this.loadProjectEmployees(this.project.id);
          this.loadAllEmployees();
        },
        error: (response) => {
          console.log(response);
        }
      });
  }
}
