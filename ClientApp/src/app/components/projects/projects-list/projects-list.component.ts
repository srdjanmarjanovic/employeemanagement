import { Component } from '@angular/core';
import {Project} from "../../../models/project.model";
import {ProjectsService} from "../../../services/projects.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent {
  projects: Project[] = [];

  constructor(
    private projectsService: ProjectsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.projectsService.getAllProjects().subscribe({
      next: (projects) => {
        this.projects = projects;
      },
      error: (response) => {
        console.log(response);
      }
    })
  }

  deleteProject(id: string) {
    this.projectsService.deleteProject(id)
      .subscribe({
        next: (response) => {
          this.projects = this.projects.filter(employee => employee.id !== id);
          alert('Project deleted!')
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
