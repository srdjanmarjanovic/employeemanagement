import { Component } from '@angular/core';
import {Employee} from "../../../models/emplyee.model";
import {EmployeesService} from "../../../services/employees.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent {
  employee: Employee = {
    id: '',
    name: '',
    email: '',
    phone: '',
    salary: 0,
    department: ''
  };

  constructor(
    private employeesService: EmployeesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        const id = params.get('id');

        if (id) {
          this.employeesService.getEmployee(id).subscribe({
            next: (employee) => {
              this.employee = employee;
            },
            error: (response) => {
              console.log(response);
            }
          })
        }
      }
    });
  }
  updateEmployee() {
    this.employeesService.updateEmployee(this.employee)
      .subscribe({
        next: (employee) => {
          alert('Employee saved!')
          this.employee = employee;
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
