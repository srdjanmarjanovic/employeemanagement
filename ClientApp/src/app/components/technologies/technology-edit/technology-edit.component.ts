import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Technology} from "../../../models/technology.model";
import {TechnologiesService} from "../../../services/technologies.service";

@Component({
  selector: 'app-technology-edit',
  templateUrl: './technology-edit.component.html',
  styleUrls: ['./technology-edit.component.css']
})
export class TechnologyEditComponent {
  technology: Technology = {
    id: '',
    name: ''
  };

  constructor(
    private technologiesService: TechnologiesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        const id = params.get('id');

        if (id) {
          this.technologiesService.getTechnology(id).subscribe({
            next: (technology) => {
              this.technology = technology;
            },
            error: (response) => {
              console.log(response);
            }
          })
        }
      }
    });
  }
  updateTechnology() {
    this.technologiesService.updateTechnology(this.technology)
      .subscribe({
        next: (technology) => {
          alert('Technology saved!')
          this.technology = technology;
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
