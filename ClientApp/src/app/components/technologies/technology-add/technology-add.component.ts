import { Component } from '@angular/core';
import {Employee} from "../../../models/emplyee.model";
import {EmployeesService} from "../../../services/employees.service";
import {Router} from "@angular/router";
import {Technology} from "../../../models/technology.model";
import {TechnologiesService} from "../../../services/technologies.service";

@Component({
  selector: 'app-technology-add',
  templateUrl: './technology-add.component.html',
  styleUrls: ['./technology-add.component.css']
})
export class TechnologyAddComponent {
  addTechnologyRequest: Technology = {
    id: '',
    name: '',
  }
  constructor(
    private technologyService: TechnologiesService,
    private router: Router
  ) {}

  addTechnology() {
    this.technologyService.addTechnology(this.addTechnologyRequest)
      .subscribe({
        next: (employee) => {
          this.router.navigate(['technologies'])
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
