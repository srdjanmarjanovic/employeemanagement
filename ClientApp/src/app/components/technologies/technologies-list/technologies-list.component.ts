import { Component } from '@angular/core';
import {Technology} from "../../../models/technology.model";
import {TechnologiesService} from "../../../services/technologies.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-technologies-list',
  templateUrl: './technologies-list.component.html',
  styleUrls: ['./technologies-list.component.css']
})
export class TechnologiesListComponent {
  technologies: Technology[] = [];

  constructor(
    private technologiesService: TechnologiesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.technologiesService.getAllTechnologies().subscribe({
      next: (technologies) => {
        this.technologies = technologies;
      },
      error: (response) => {
        console.log(response);
      }
    })
  }

  deleteTechnology(id: string) {
    this.technologiesService.deleteTechnology(id)
      .subscribe({
        next: (response) => {
          this.technologies = this.technologies.filter(technology => technology.id !== id);
          alert('Technology deleted!')
        },
        error: (response) => {
          console.log(response);
        }
      })
  }
}
