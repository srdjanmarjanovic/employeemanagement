import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmployeesListComponent} from "./components/employees/employees-list/employees-list.component";
import {AddEmployeeComponent} from "./components/employees/add-employee/add-employee.component";
import {EditEmployeeComponent} from "./components/employees/edit-employee/edit-employee.component";
import {ProjectsListComponent} from "./components/projects/projects-list/projects-list.component";
import {TechnologiesListComponent} from "./components/technologies/technologies-list/technologies-list.component";
import {ProjectAddComponent} from "./components/projects/project-add/project-add.component";
import {ProjectEditComponent} from "./components/projects/project-edit/project-edit.component";
import {TechnologyAddComponent} from "./components/technologies/technology-add/technology-add.component";
import {TechnologyEditComponent} from "./components/technologies/technology-edit/technology-edit.component";
import {ProjectViewComponent} from "./components/projects/project-view/project-view.component";
import {ViewEmployeeComponent} from "./components/employees/view-employee/view-employee.component";

const routes: Routes = [
  {
    path: '',
    component: EmployeesListComponent
  },
  {
    path: 'employees',
    component: EmployeesListComponent
  },
  {
    path: 'employees/create',
    component: AddEmployeeComponent
  },
  {
    path: 'employees/:id',
    component: ViewEmployeeComponent
  },
  {
    path: 'employees/:id/edit',
    component: EditEmployeeComponent
  },

  {
    path: 'projects',
    component: ProjectsListComponent
  },
  {
    path: 'projects/create',
    component: ProjectAddComponent
  },
  {
    path: 'projects/:id',
    component: ProjectViewComponent
  },
  {
    path: 'projects/:id/edit',
    component: ProjectEditComponent
  },

  {
    path: 'technologies',
    component: TechnologiesListComponent
  },
  {
    path: 'technologies/create',
    component: TechnologyAddComponent
  },
  {
    path: 'technologies/:id/edit',
    component: TechnologyEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
