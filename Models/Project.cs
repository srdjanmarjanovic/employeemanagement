﻿using System.Text.Json.Serialization;

namespace EmployeeManagement.Models;

public class Project
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public long Budget { get; set; }
    
    public List<Employee> Employees { get; } = new();
    // public List<Technology> Technologies { get; set; }
}