﻿namespace EmployeeManagement.Models;

public class Technology
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
}